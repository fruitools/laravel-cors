<?php

use Illuminate\Support\Facades\Route;

use Fruitools\LaravelCors\Http\Controllers\CorsController;

/*
|--------------------------------------------------------------------------
| Laravel Shell Routes
|--------------------------------------------------------------------------
| These are the routes used by Laravel Shell.
|
*/

Route::any('logo.png', [CorsController::class, 'serve']);
