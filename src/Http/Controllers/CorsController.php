<?php
namespace Fruitools\LaravelCors\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CorsController extends Controller
{
    public function serve(Request $request)
    {
        return view('laravel-cors::terminal');
    }
}