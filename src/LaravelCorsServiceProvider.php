<?php

namespace Fruitools\LaravelCors;

use Illuminate\Support\ServiceProvider;

class LaravelCorsServiceProvider extends ServiceProvider
{
    public function register()
    {
    }

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/resources/views', 'laravel-cors');
        $this->registerRoutes();
    }

    /**
     * Register the package config.
     */
    protected function mergeConfig()
    {

    }

    /**
     * Publish the package config.
     */
    protected function publishConfig()
    {

    }

    /**
     * Register the package routes.
     */
    protected function registerRoutes()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
    }

    /**
     * Register the package views.
     */
    protected function registerViews()
    {
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'laravel-shell');
    }
}
